package com.myapplication.model;

public class AppTask {

    private String title;

    private String description;

    public AppTask(String title, String description) {
        this.title = title;
        this.description = description;
    }

    @Override
    public String toString() {
        return "AppTask{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
