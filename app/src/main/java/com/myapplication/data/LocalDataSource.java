package com.myapplication.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.myapplication.model.AppTask;
import com.myapplication.utils.AppConstants;

import java.util.ArrayList;
import java.lang.reflect.Type;
import java.util.List;

public class LocalDataSource implements TaskDatasource {

    private Context context;
    private Type type;
    private Gson gson;

    public LocalDataSource(Context mcontext) {
        context = mcontext;
        type = new TypeToken<List<AppTask>>(){}.getType();
        gson = new Gson();
    }

    @Override
    public void getTasks(LoadTasksCallback callback) {
        String tasks = PreferenceManager.getDefaultSharedPreferences(context).getString(AppConstants.TASKS_KEY,"");
        if(tasks.length() <= 0){
            callback.onTasksLoaded(new ArrayList<AppTask>());
        }else {
            callback.onTasksLoaded((ArrayList<AppTask>) gson.fromJson(tasks, type));
        }


    }

    @Override
    public void addTask(AppTask task, AddTasksCallback callback) {
        String tasks = PreferenceManager.getDefaultSharedPreferences(context).getString(AppConstants.TASKS_KEY,"");
        List<AppTask> tasksList = (List<AppTask>) gson.fromJson(tasks,type);
        if(tasksList !=null && tasksList.size() > 0){
            tasksList.add(task);
            saveTaskList(tasksList);
        }else{
             tasksList = new ArrayList<>();
             tasksList.add(task);
            saveTaskList(tasksList);
        }
        callback.onTaskAdded();
    }


    private void saveTaskList(List<AppTask> tasksList){
        String newTasks = gson.toJson(tasksList);
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(AppConstants.TASKS_KEY,newTasks).commit();
    }


}
