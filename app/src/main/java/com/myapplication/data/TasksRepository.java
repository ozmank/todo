package com.myapplication.data;

import android.content.Context;

import com.myapplication.model.AppTask;

import java.util.List;

public class TasksRepository implements TaskDatasource {

    private static TasksRepository INSTANCE = null;
    private Context context;
    private LocalDataSource dataSource;

    private  TasksRepository(Context mContext) {
        this.context = mContext;
        dataSource = new LocalDataSource(context);
    }


    public static TasksRepository getInstance(Context context){
        if(INSTANCE == null){
            INSTANCE = new TasksRepository(context);
        }
        return INSTANCE;
    }



    public static void destroyInstance() {
        INSTANCE = null;
    }


    @Override
    public void getTasks(LoadTasksCallback callback) {
         dataSource.getTasks(callback);
    }

    @Override
    public void addTask(AppTask task, AddTasksCallback callback) {
            dataSource.addTask(task,callback);
    }


}
