package com.myapplication.data;

import com.myapplication.model.AppTask;

import java.util.ArrayList;
import java.util.List;

public interface TaskDatasource {

    interface LoadTasksCallback {

        void onTasksLoaded(ArrayList<AppTask> tasks);

        void onDataNotAvailable();
    }

    interface AddTasksCallback{

        void onTaskAdded();
    }


    void getTasks( LoadTasksCallback callback);


    void addTask(AppTask task,AddTasksCallback callback);




}
