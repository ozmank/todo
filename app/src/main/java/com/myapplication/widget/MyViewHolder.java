package com.myapplication.widget;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.myapplication.R;

public class MyViewHolder extends RecyclerView.ViewHolder {
    public TextView title;
    public TextView description;




    public MyViewHolder(View itemView) {
        super(itemView);

        title = itemView.findViewById(R.id.title);
        description = itemView.findViewById(R.id.desc);
    }
}
