package com.myapplication.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.myapplication.R;
import com.myapplication.model.AppTask;

import java.util.ArrayList;

public class TasksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<AppTask> tasks;
    private Context context;

    public TasksAdapter(ArrayList<AppTask> tasks, Context context) {
        this.tasks = tasks;
        this.context = context;
        setHasStableIds(true);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            AppTask task = tasks.get(position);
        MyViewHolder contentItem = (MyViewHolder) holder;
        contentItem.title.setText(position + ":" + " Title " +  task.getTitle());
        contentItem.description.setText("Description: " + task.getDescription());
    }

    @Override
    public int getItemCount() {
        return tasks.size();
    }


    public void setTasks(ArrayList<AppTask> mTasks){
        tasks = mTasks;
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return (long)position;
    }
}
