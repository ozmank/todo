package com.myapplication.addTask;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.myapplication.R;
import com.myapplication.data.TaskDatasource;
import com.myapplication.data.TasksRepository;
import com.myapplication.model.AppTask;
import com.myapplication.tasks.TasksActivity;
import com.myapplication.tasks.TasksContract;

public class AddTaskActivity extends AppCompatActivity implements AddTaskContract.View {
    private AddTaskContract.Presenter presenter;
    private ActionBar mActionBar;
    private Snackbar snackbar;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_task);
        presenter = new AddtaskPresenter(this, TasksRepository.getInstance(this));
        snackbar = Snackbar.make(findViewById(R.id.main), R.string.app_name,
                Snackbar.LENGTH_SHORT);

        FloatingActionButton fab =
                (FloatingActionButton)findViewById(R.id.fab_done_task);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setDisplayShowHomeEnabled(true);
        mActionBar.setTitle("Add TO-DO");
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        fab.setImageResource(R.drawable.ic_done);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText title = findViewById(R.id.add_task_title);
                EditText desc = findViewById(R.id.add_task_description);
                String sTitle = title.getText().toString().trim();
                String sDesc = desc.getText().toString().trim();


                    if(presenter.validateInput(sTitle)) {
                        presenter.addTask(new AppTask(sTitle, sDesc));
                    }
            }
        });


    }

    @Override
    public void setPresenter(TasksContract.Presenter presenter) {

    }

    @Override
    public void showInvalidInput() {
        snackbar.setText("Title must not be empty").show();
    }

    @Override
    public void showTaskAdded() {
        snackbar.setText("TO-DO added").show();
        finish();
    }
}
