package com.myapplication.addTask;

import com.myapplication.base.BasePresenter;
import com.myapplication.base.BaseView;
import com.myapplication.data.TaskDatasource;
import com.myapplication.model.AppTask;
import com.myapplication.tasks.TasksContract;

public interface AddTaskContract {

    interface View extends BaseView<TasksContract.Presenter> {
            void showInvalidInput();
            void showTaskAdded();
    }


    interface Presenter extends BasePresenter {
        boolean validateInput(String title);

        void addTask(AppTask task);
    }
}
