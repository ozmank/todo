package com.myapplication.addTask;

import com.myapplication.data.TaskDatasource;
import com.myapplication.data.TasksRepository;
import com.myapplication.model.AppTask;

public class AddtaskPresenter implements AddTaskContract.Presenter {

    private AddTaskContract.View view;
    private TasksRepository repo;

    public AddtaskPresenter(AddTaskContract.View view, TasksRepository repository) {
        this.view = view;
        repo = repository;
    }


    @Override
    public void start() {

    }


    @Override
    public boolean validateInput(String title) {
        if(title.length() > 0){
            return true;
        }else{
        view.showInvalidInput();
        return false;
        }
    }

    @Override
    public void addTask(AppTask task) {
        repo.addTask(task, new TaskDatasource.AddTasksCallback() {
            @Override
            public void onTaskAdded() {
                view.showTaskAdded();
            }
        });
    }


}
