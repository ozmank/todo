package com.myapplication.tasks;

import com.myapplication.data.TaskDatasource;
import com.myapplication.data.TasksRepository;
import com.myapplication.model.AppTask;

import java.util.ArrayList;
import java.util.List;

public class TasksPresenter implements TasksContract.Presenter {
    private TasksContract.View tasksView;
    private boolean isLoading;
    private TasksRepository repository;

    public TasksPresenter(TasksContract.View tasksView, TasksRepository mRepository) {
        this.tasksView = tasksView;
        isLoading = false;
        repository = mRepository;
        tasksView.setPresenter(this);
    }


    @Override
    public void loadTasks() {

        if(!isLoading){
                isLoading = true;
                repository.getTasks(new TaskDatasource.LoadTasksCallback() {
                    @Override
                    public void onTasksLoaded(ArrayList<AppTask> tasks) {
                        isLoading = false;
                        if(tasks.size() > 0) {
                            tasksView.showTasks(tasks);
                        }else{
                            tasksView.showNoTasks();
                        }
                    }

                    @Override
                    public void onDataNotAvailable() {
                        isLoading = false;
                            tasksView.showErrorLoadingTaks();
                    }
                });
        }

    }

    @Override
    public void start() {
        loadTasks();
    }
}
