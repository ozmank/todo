package com.myapplication.tasks;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.myapplication.R;
import com.myapplication.addTask.AddTaskActivity;
import com.myapplication.data.TasksRepository;
import com.myapplication.model.AppTask;
import com.myapplication.widget.TasksAdapter;

import java.util.ArrayList;
import java.util.List;

import static android.widget.LinearLayout.HORIZONTAL;

public class TasksActivity extends AppCompatActivity implements TasksContract.View {
    private TasksPresenter presenter;
    private RecyclerView recyclerView;
    private Snackbar snackbar;
    private TasksAdapter adapter;
    private ActionBar mActionBar;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        presenter = new TasksPresenter(this, TasksRepository.getInstance(this));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mActionBar = getSupportActionBar();

        mActionBar.setTitle("TO-DO App");
        recyclerView = findViewById(R.id.recyclerView);
        DividerItemDecoration itemDecor = new DividerItemDecoration(this, HORIZONTAL);
        recyclerView.addItemDecoration(itemDecor);
        snackbar = Snackbar.make(findViewById(R.id.myCoordinatorLayout), R.string.app_name,
                Snackbar.LENGTH_SHORT);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new TasksAdapter(new ArrayList<AppTask>(),this);
        recyclerView.setAdapter(adapter);

        FloatingActionButton fab =
                (FloatingActionButton)findViewById(R.id.fab_add_task);

        fab.setImageResource(R.drawable.ic_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TasksActivity.this, AddTaskActivity.class));
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.start();
    }

    @Override
    public void setLoadingIndicator() {
        snackbar.setText(R.string.loading_tasks).show();
    }

    @Override
    public void showTasks(ArrayList<AppTask> tasks) {
       int size = tasks.size();
       adapter.setTasks(tasks);
       adapter.notifyDataSetChanged();

    }

    @Override
    public void showNoTasks() {
        snackbar.setText(R.string.no_tasks_added).show();
    }

    @Override
    public void showErrorLoadingTaks() {
        snackbar.setText(R.string.error_fetching).show();
    }

    @Override
    public void setPresenter(TasksContract.Presenter mPresenter) {

    }
}
