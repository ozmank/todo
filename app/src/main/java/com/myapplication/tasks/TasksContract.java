package com.myapplication.tasks;

import com.myapplication.base.BasePresenter;
import com.myapplication.base.BaseView;
import com.myapplication.model.AppTask;

import java.util.ArrayList;
import java.util.List;

public interface TasksContract {


    interface View extends BaseView<Presenter> {


        void setLoadingIndicator();

        void showTasks(ArrayList<AppTask> tasks);


        void showNoTasks();

        void showErrorLoadingTaks();

    }


    interface Presenter extends BasePresenter {
        void loadTasks();
    }
}
