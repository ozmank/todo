package com.myapplication.base;

public interface BaseView<T extends BasePresenter> {

    void setPresenter(T presenter);
}
