package com.myapplication.base;

public interface BasePresenter {
    void start();
}
